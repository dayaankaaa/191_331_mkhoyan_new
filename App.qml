import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtQuick.Controls.Material 2.3
import QtMultimedia 5.12
import QtQuick.Dialogs 1.2
import QtQuick.Controls.Styles 1.4
import QtGraphicalEffects 1.0
import QtQuick 2.0
import QtQuick.Window 2.12
import QtQml 2.12
import QtWebView 1.1
import QtWebSockets 1.1

Page{
    id: app
    header:   Rectangle {


        Connections {
            target: httpController // Указываем целевой объект для соединения

            onTokenToQml: {
                tokenShow.text = token // Устанавливаем аву
            }

            onDataToQml: {
                base64data.text = pageContent;
            }
        }

        AnimatedImage {
            id: animatedImage
            x: 8
            y: 8
            width: 75
            height: 75
            source: "politech.png"
        }

        Label {
            anchors.left: animatedImage.right
            anchors.leftMargin: 20
            anchors.horizontalCenter: header.horizontalCenter
            anchors.verticalCenter: header.verticalCenter
            x: 16
            y: 0
            text: qsTr("ПУБЛИКАЦИИ")
            font.pixelSize: 30
            styleColor: "#000000"
            padding: 10

        }
        id: rectangleHeader
        x: 0
        y: 0
        width: 600
        height: 95
        anchors.right: parent.right
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#1262cb"
            }

            GradientStop {
                position: 0.98157
                color: "#1262cb"
            }



        }
        anchors.left: parent.left
    }
    ScrollView {
        id: view
        anchors.fill: parent
        ColumnLayout{
        RowLayout{
            CheckBox{

            }

            Image {
                width: 40
                height: 40
                fillMode: Image.PreserveAspectFit
                id: img
                source: "politech.png"
            }
            ColumnLayout{
            Text {
                id: name
                text: qsTr("Заголовок")
            }
            TextArea{
                text: "TextArsfhksdjfhksdjfhskjdfhks jd  fh sk ea\n...\n...\n...\n...\n..."
            }
            Text {
                id: date
                text: qsTr("да:та")
            }
        }
            ColumnLayout{
                Image {
                    width: 10
                    height: 10
                    fillMode: Image.PreserveAspectFit
                    id: img1
                    source: "up.png"
                }
                Text {
                    id: mun
                    text: qsTr("количество голосов")
                }
                Image {
                    width: 10
                    height: 10
                    fillMode: Image.PreserveAspectFit
                    id: img2
                    source: "down.png"
                }
            }
      }
        RowLayout{
            CheckBox{

            }

            ColumnLayout{
            Text {
                id: name2
                text: qsTr("Заголовок")
            }
            TextArea{
                text: "TextArsfhksdjfhksdjfhskjdfhks jd  fh sk ea\n...\n...\n...\n...\n..."
            }
            Text {
                id: date3
                text: qsTr("да:та")
            }
        }
            ColumnLayout{
                Image {
                    width: 10
                    height: 10
                    fillMode: Image.PreserveAspectFit
                    id: iqq
                    source: "up.png"
                }
                Text {
                    id: mun4
                    text: qsTr("количество голосов")
                }
                Image {
                    width: 10
                    height: 10
                    fillMode: Image.PreserveAspectFit
                    id: img7
                    source: "down.png"
                }
            }
      }
        RowLayout{
            CheckBox{

            }

            Image {
                width: 40
                height: 40
                fillMode: Image.PreserveAspectFit
                id: img5
                source: "politech.png"
            }
            ColumnLayout{
            Text {
                id: namef
                text: qsTr("Заголовок")
            }
            TextArea{
                text: "TextArsfhksdjfhksdjfhskjdfhks jd  fh sk ea\n...\n...\n...\n...\n..."
            }
            Text {
                id: datef
                text: qsTr("да:та")
            }
        }
            ColumnLayout{
                Image {
                    width: 10
                    height: 10
                    fillMode: Image.PreserveAspectFit
                    id: imf1
                    source: "up.png"
                }
                Text {
                    id: munff
                    text: qsTr("количество голосов")
                }
                Image {
                    width: 10
                    height: 10
                    fillMode: Image.PreserveAspectFit
                    id: imgsdf
                    source: "down.png"
                }
            }
      }
    }
}
}
