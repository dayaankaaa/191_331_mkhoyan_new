#ifndef VKMODEL_H
#define VKMODEL_H

#include <QObject>
#include <QAbstractItemModel>
#include<QAbstractListModel>
#include <QList>
#include <QModelIndex>
#include <QVariant>
#include <QUrl>
#include <QVariant>

class vkObject
{
public:

    vkObject(
               const QUrl &photo,
               const QString &name,
               const QString &text,
               const QString &date);
    //
    QUrl getPhoto() const;
    QString getName() const;
    QString getText() const;
    QString getDate() const;
    // прочие get-методы для выдачи ID, URL фотографии и др.
private:
    QUrl m_photo;
    QString m_name;
    QString m_text;
    QString m_date;
    // прочие свойства для хранения ID, URL фотографии и др.
};

class vkModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum enmRoles {
        Photo,
        Name,
        Text,
        Date
    };

    vkModel(QObject *parent = nullptr);

    void addItem(const vkObject & newItem);

    int rowCount(const QModelIndex & parent = QModelIndex()) const;

    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const; // возвращает по индексу переменную (импользуется в ЛР 8)

    QVariantMap get(int idx) const;

    void clear();

protected:
    QHash<int, QByteArray> roleNames() const;
    // ключ - значение
    // нужен, чтобы строковые имена приводить в соответствие к полям френда

private:
    QList<vkObject> m_items;
};
#endif // VKMODEL_H
