#include "vkmodel.h"
#include <QAbstractItemModel>
#include <QMap>

vkObject::vkObject( const QUrl &getPhoto, const QString &getName, const QString &getText, const QString &getDate)
    :
        m_photo(getPhoto),
        m_name(getName),
        m_text(getText),
        m_date(getDate)
{
}

vkModel::vkModel(QObject *parent) : QAbstractListModel(parent)
{
}


QUrl vkObject::getPhoto() const{
    return m_photo;
}

QString vkObject::getName() const{
    return m_name;
}

QString vkObject::getDate() const{
    return m_date;
}


QString vkObject::getText() const{
    return m_text;
}


void vkModel::addItem(const vkObject & newItem){
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_items << newItem;
    endInsertRows();
}

int vkModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_items.count();
}

QVariant vkModel::data(const QModelIndex & index,
                                int role) const
{
    if (index.row() < 0 || (index.row() >= m_items.count()))
        return QVariant();

    const vkObject&itemToReturn = m_items[index.row()];
     if (role == Photo)
    return itemToReturn.getPhoto();
    else if (role == Name)
    return itemToReturn.getName();
    else if (role == Text)
    return itemToReturn.getText();
    else if (role == Date)
    return itemToReturn.getDate();

    return QVariant();
}

QHash<int, QByteArray> vkModel::roleNames() const
{
    QHash<int, QByteArray> roles;

    roles[Photo] = "pict";
    roles[Name] = "name";
    roles[Date] = "date";
    roles[Text] = "text";
    return roles;
}

QVariantMap vkModel::get(int idx) const
{
    QVariantMap map;
    foreach(int k, roleNames().keys())
    {
        map[roleNames().value(k)] = data(index(idx, 0), k);
    }
    return map;
}

void vkModel::clear()
{
    beginRemoveRows(QModelIndex(), 0, rowCount()-1);
    m_items.clear();
    endRemoveRows();
}
