import QtQuick 2.4
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import QtQuick.Controls.Material 2.3
import QtMultimedia 5.12
import QtQuick.Dialogs 1.0

ApplicationWindow {
    width: 640
    height: 480
    visible: true
    title: qsTr("Tabs")

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        Auth {
            id: auth
        }

        App {
            id: app
        }

    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex

        TabButton {
            text: qsTr("Auth")
        }
        TabButton {
            text: qsTr("App")
        }
    }

    Drawer{
    id: drawer
    width: 0.6 * parent.width
    height: parent.height
    Material.background: "#1e1e1e"

    GridLayout{
    width: parent.width
    columns: 1

    Image {
        Layout.alignment: Qt.AlignCenter
        Layout.maximumWidth: parent.width/1.5
        Layout.maximumHeight: parent.width/1.5
        fillMode: Image.PreserveAspectCrop
        id: logoPolytech
        source: "logo.png"
    }

    Label{
    id: name
    anchors.top: logo.bottom
    text: "     Рабочий макет \n    мобильного клиента
    блога - 2"
    anchors.horizontalCenter: parent.horizontalCenter
    font.pixelSize: 20

    color: "#1262cb"
    }

    Label{
    id: discname
    anchors.top: name.bottom
    anchors.topMargin: 8
    text: "Разработка безопасных \n сетевых приложений"
    anchors.horizontalCenter: parent.horizontalCenter
    font.pixelSize: 15
    color: "#ffffff"
    }


    Label{
    anchors.top: discname.bottom
    anchors.topMargin: 10
    text: "Мхоян Даяна 191-331"
    anchors.horizontalCenter: parent.horizontalCenter
    font.pixelSize: 25
    color: "#1262cb"
    }

    Text {
                    text: "<a href='https://gitlab.com/dayaankaaa/191_331_mkhoyan.git'>https://gitlab.com/dayaankaaa/191_331_mkhoyan.git</a>";
                    id: text
                    wrapMode: Text.WrapAnywhere

                    onLinkActivated:
                    {
                        console.log(link)
                        Qt.openUrlExternally(link)
                    }

                }

    Button{
    text: "АВТОРИЗАЦИЯ"

    flat: true
    onClicked: {
    swipeView.currentIndex = 0
    drawer.close()
    }
    }

    Button{
    text: "ЛЕНТА"
    flat: true
    onClicked: {
    swipeView.currentIndex = 1
    drawer.close()
    }
    }
    }
    }
    // #1262cb
}
